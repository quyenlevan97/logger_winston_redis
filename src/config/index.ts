import dotenv from 'dotenv';
import path from 'path';
//set the NODE_ENV to 'development' by default
process.env.NODE_ENV=process.env.NODE_ENV || 'development';
const envPath=process.env.NODE_ENV ==='development' ?'.env' :`.env.${process.env.NODE_ENV}`

//config dotenv get .env file by specify path
const envFound=dotenv.config({path:path.join(__dirname,`../../${envPath}`)});

//config read .env file if it can't read the propertity into .env file it will accounce error
if(envFound.error){
    throw new Error('Couldn\'t file .env file');
}


export default{
    port : Number(process.env.PORT),
    redis:{
        host:process.env.REDIS_HOST,
        port:Number(process.env.REDIS_PORT),
        auth:process.env.REDIS_AUTH,
        db:Number(process.env.REDIS_DB)
    },



    logs:{
        level :process.env.LOG_LEVEL || 'silly'
    }
}