import path from 'path';
import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';


const options = {
    infoFile: {
        level: 'info',
        name: 'file.info',
        filename: path.resolve(__dirname, '../', 'logs', '%DATE%.log'),
        handleExceptions: true,
        json: true,
        colorsize: true,
        datePattern: 'YYYY-MM-DD-HH',
        maxsize: 5242880,
        maxFiles: '14d',
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true
    }
}

winston.transports.DailyRotateFile = DailyRotateFile;

const LoggerInstance = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(info => {
            return `[${info.timestamp}] [${info.level}] : ${typeof info.message === 'object' ? JSON.stringify(info.message) : info.message}`;
        })
    ),
    transports: [
        new winston.transports.Console(options.console),
        new winston.transports.DailyRotateFile(options.infoFile),
    ],
    exitOnError:false,
})


export default LoggerInstance;