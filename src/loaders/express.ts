import express from 'express';
import logger from 'morgan';
export default ({app}:{app:express.Application})=>{
    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({extended:true}));
    app.get("/status",(req,res)=>{
        res.status(200).end();
    })
    app.get("/",(req,res)=>{
        res.send("vao day choi ne");
    })

}
