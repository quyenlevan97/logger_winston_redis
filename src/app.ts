import express from 'express';
import Logger from './loaders/logger';
import Loaders from './loaders';
import config from './config';
async function startServer() {
    console.log(process.env.NODE_ENV);
    const app=express();
    await Loaders({expressApp:app});
    app.listen(config.port,()=>{
        Logger.info(`Server listening on port : ${config.port}`);
    })
}
startServer();